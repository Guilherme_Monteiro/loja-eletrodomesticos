/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author aluno
 */
public class Listareletro {

    public void listarTeste(JList listaAlimentacao) {
        DefaultListModel teste = (DefaultListModel) listaAlimentacao.getModel();
        teste.addElement(new String("Teste"));
    }

    public void listar(JList listaAlimentacao) {
        try {
            //listaAlimentacao.removeAll();
            DefaultListModel dfm = new DefaultListModel();
            Connection c = Conexao.obterConexao();
            String SQL = "SELECT * FROM sc_loja_eletrodomesticos.loja";

            //PreparedStatement ps = c.prepareStatement(SQL);
            //ResultSet rs = ps.executeQuery(SQL);
            System.out.println(c);
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(SQL);
            while (rs.next()) {
                dfm.addElement(rs.getString("nome"));
                System.out.println(rs.getString("nome"));
            }
            listaAlimentacao.setModel(dfm);
            //JOptionPane.showConfirmDialog(null, "Oiiiiiiii de novo", "Teste", JOptionPane.OK_OPTION);            
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Listareletro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void detalhar(JTable Tabela) {
        try {
            DefaultTableModel dtm = new DefaultTableModel();
            dtm = (DefaultTableModel) Tabela.getModel();
            Connection c = Conexao.obterConexao();
            String SQL = "SELECT * FROM sc_loja_eletrodomesticos.loja";
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(SQL);
            while (rs.next()) {
                dtm.addRow(new Object[]{
                    rs.getString("nome"), rs.getString("email"), rs.getString("CPF"),
                    rs.getString("idade")});
                c.close();
            }

        } catch (SQLException ex) {
            Logger.getLogger(Listagem.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void selecaoAluno(JComboBox combo) {
        try {
            DefaultComboBoxModel m = new DefaultComboBoxModel();
            Connection c = Conexao.obterConexao();
            PreparedStatement p = c.prepareStatement("Select * FROM SC_loja_eletrodomesticos.loja");
            ResultSet rs = p.executeQuery();
            while (rs.next()) {
                Perfil per = new Perfil(rs.getString("cpf"),rs.getString("nome"));
                m.addElement(per);
               
            }
            combo.setModel(m);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Listagem.class.getName()).log(Level.SEVERE, null, ex);
        } 

    }    
}
