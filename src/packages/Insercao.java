/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package packages;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aluno
 */
public class Insercao {

    public void Inserir(String nome, String email, String CPF, String idade) {

        try {
            Connection c = Conexao.obterConexao();

            PreparedStatement ps = c.prepareStatement("insert into sc_loja_eletrodomesticos.loja(nome, email, CPF, idade) values(?,?,?,?)");

            ps.setString(1, nome);
            ps.setString(2, email);
            ps.setString(3, CPF);
            ps.setString(4, idade);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public void InserirPag(String id, String tipo, String CPF) {

        try {
            Connection c = Conexao.obterConexao();

            PreparedStatement ps = c.prepareStatement("insert into sc_loja_eletrodomesticos.pag(id, tipo, CPF) values(?,?,?)");

            ps.setString(1, id);
            ps.setString(2, tipo);
            ps.setString(3, CPF);
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    //public void InserirDieta(String tipo, String alergias, String alimentosrecorrentes){
    /*try {
            Connection c=Conexao.obterConexao();
            PreparedStatement ps = null;
            try {
                ps = c.prepareStatement("insert into sc_loja_eletrodomesticos(, alergias, alimentosrecorrentes) values(?,?,?)");
            } catch (SQLException ex) {
                Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                ps.setString(1, tipo);
            } catch (SQLException ex) {
                Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                ps.setString(2, alergias);
            } catch (SQLException ex) {
                Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                ps.setString(3, alimentosrecorrentes);
            } catch (SQLException ex) {
                Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
            }
            ps.executeQuery();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     */
}
