
package packages;
import javax.swing.*;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aluno
 */
public class ListarTema {
    public void listar(JList listaEletrodomesticos){
        try{
            listaEletrodomesticos.removeAll();
            
            DefaultListModel d1m = new DefaultListModel();
            Connection c = Conexao.obterConexao();
            String SQL = "select * from sc_loja_eletrodomesticos.loja";
            
            PreparedStatement ps = c.prepareStatement(SQL);
            ResultSet rs = ps.executeQuery(SQL);
            
            while(rs.next()){
                d1m.addElement(rs.getString("nome_e"));
            }
            listaEletrodomesticos.setModel(d1m);
            c.close();
            
        }catch(SQLException ex){
            Logger.getLogger(ListarTema.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
